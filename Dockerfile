FROM python:3.9.7-buster

WORKDIR /opt

COPY app.py .
COPY requirements.txt .

RUN python --version
RUN pip install -U pip
RUN pip install -r requirements.txt
ENTRYPOINT ["python", "app.py"]
