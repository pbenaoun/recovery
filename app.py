import asyncio
import json
import logging
import os
import time
from datetime import datetime, timedelta

import boto3
import httpx
import re
import urllib3
from dotenv import load_dotenv

start_time = time.time()
chunk_size = int(os.getenv('CHUNK_SIZE', '1000'))
transport = httpx.AsyncHTTPTransport(retries=10, verify=False)
timeout = httpx.Timeout(30.0, connect=30.0)
headers = {
    'Content-Type': 'application/json',
    'user-agent': 'es-recover/0.0.1',
}


async def call(payload, fluentd_url):
    try:
        async with httpx.AsyncClient(transport=transport, timeout=timeout) as client:
            res = await client.post(fluentd_url, json=payload, headers=headers)
            res.raise_for_status()
    except httpx.HTTPStatusError as error:
        logging.error("Error (%s-%s) while requesting" % (error.response.status_code, error.response.reason_phrase))
    except httpx.TimeoutException:
        logging.error("Error Timeout Connection")
    except httpx.TransportError:
        logging.error("Error Transport Connection")
    except httpx.HTTPError:
        logging.error("Error while requesting HTTPError")


async def split_request(collection_line, fluentd_url):
    chunked_list = [collection_line[i:i + chunk_size] for i in range(0, len(collection_line), chunk_size)]
    for chunk in chunked_list:
        await call(chunk, fluentd_url)


def url_matcher(url):
    regex = re.compile(r"^.*health.*$")
    if regex.match(url) is not None:
        return True
    else:
        return False


async def main():
    load_dotenv('../.env')
    urllib3.disable_warnings()
    level = os.environ.get('LOG_LEVEL', 'INFO').upper()
    logging.basicConfig(format='%(asctime)s %(message)s', level=level)
    fluentd_url = {"external": os.environ['FLUENT_ENDPOINT'], "internal": os.environ['FLUENT_ENDPOINT_INTERNAL']}
    my_bucket = os.environ['S3_BUCKET']
    scope = os.environ['KONG_SCOPE']
    s3_client = boto3.client("s3")

    start_env = os.environ['START_DATE']
    end_env = os.environ['END_DATE']
    start_date_obj = datetime.fromisoformat(start_env)
    end_date_obj = datetime.fromisoformat(end_env)

    if end_date_obj < start_date_obj:
        logging.info('end_date_obj < start_date_obj')
        quit()

    date_list = date_range_list(start_date_obj, end_date_obj)
    for current in date_list:
        day = current.strftime("%Y-%m-%d")
        date_prefix = 'logs/kong.%s.log/%s/%s/' % (scope, current.year, day)
        logging.info('Recover KONG logs from S3 for %s' % day)
        response = s3_client.list_objects_v2(Bucket=my_bucket, Prefix=date_prefix)
        if 'Contents' in response:
            files = [key['Key'] for key in response['Contents']]
            for doc in files:
                logging.info('handling file %s' % doc)
                try:
                    s3_client = boto3.client("s3")
                    obj = s3_client.get_object(Bucket=my_bucket, Key=doc)
                    body = obj['Body'].read().decode("utf-8")
                    lines = body.split('\n')
                    collection_line = []

                    for line in lines:
                        if '{' in line:
                            body = json.loads(line)
                            date_string = body['time']
                            timestamp = time.mktime(
                                datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S+00:00").timetuple())
                            body['time'] = timestamp

                            if url_matcher(body['request']['url']):
                                logging.debug('this is healthcheck payload, no recover to elasticsearch')
                            else:
                                if 'correlationId' in body:
                                    correlationId = body['correlationId']
                                    # remove if correlation id not exist
                                    if len(correlationId) != 0:
                                        collection_line.append(body)
                                else:
                                    logging.debug('payload removed: %s', (json.dumps(body)))

                    await split_request(collection_line, fluentd_url[scope])
                    logging.info('file %s integrated to fluentD' % doc)
                except Exception as e:
                    logging.info('Exception with: %s' % e)
        else:
            logging.info('No objects returned')


def date_range_list(start_date, end_date):
    # Return list of datetime.date objects between start_date and end_date (inclusive).
    date_list = []
    curr_date = start_date
    while curr_date <= end_date:
        date_list.append(curr_date)
        curr_date += timedelta(days=1)
    return date_list


if __name__ == '__main__':
    asyncio.run(main())
    logging.info("--- %s seconds ---" % (time.time() - start_time))
